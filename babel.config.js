const presets = [
  [
    "@babel/env",
    {
      targets: {
        "node": "6.14.3"
      },
    },
  ],
];

module.exports = { presets };
