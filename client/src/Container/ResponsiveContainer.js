import PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
  Button,
  Container,
  Divider,
  Grid,
  Header,
  Icon,
  Image,
  List,
  Menu,
  Responsive,
  Segment,
  Sidebar,
  Visibility,
} from 'semantic-ui-react'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

const fixed = false;
const mylinks = [
  {link:'/c/mechanics/', nombre:'Mechanics' },
  {link:'/c/items/',nombre:'Items'},
  {link:'/c/strategies/',nombre:'Strategies'}
];

class MobileContainer extends Component {
  state = {}

  handlePusherClick = () => {
    const { sidebarOpened } = this.state

    if (sidebarOpened) this.setState({ sidebarOpened: false })
  }

  handleToggle = () => this.setState({ sidebarOpened: !this.state.sidebarOpened })

  render() {
    const  {children}= this.props
    const { sidebarOpened } = this.state
    const { url } = this.props

    return (
      <Responsive maxWidth={Responsive.onlyMobile.maxWidth}>
        <Sidebar.Pushable as={Segment}>
          <Sidebar as={Menu} animation='uncover' inverted vertical visible={sidebarOpened}>
            {
            url == "/" ?

              <Menu.Item as={Link} to="/" active>
                Home
              </Menu.Item>
              :
              <Menu.Item as={Link} to="/">
                Home
              </Menu.Item>
            }

            {
              mylinks.map( (item,index) => {
                  if(url == item.link)
                    return ( <Menu.Item key={index} as={Link} to={item.link} active> {item.nombre} </Menu.Item> )
                  else {
                    return ( <Menu.Item key={index} as={Link} to={item.link}> {item.nombre} </Menu.Item> )
                  }
                }
              )
            }

            <Menu.Item as='a'>Log in</Menu.Item>
            <Menu.Item as='a'>Sign Up</Menu.Item>

          </Sidebar>

          <Sidebar.Pusher
            dimmed={ sidebarOpened }
            onClick={ this.handlePusherClick }
            style={{ minHeight: '100vh' }}
          >
            <Segment
              inverted
              textAlign='center'
              vertical
            >
              <Container>
                <Menu inverted pointing secondary size='large'>
                  <Menu.Item onClick={ this.handleToggle }>
                    <Icon name='sidebar' />
                  </Menu.Item>
                  <Menu.Item position='right'>
                    <Button as='a' inverted>
                      Log in
                    </Button>
                    <Button as='a' inverted style={{ marginLeft: '0.5em' }}>
                      Sign Up
                    </Button>
                  </Menu.Item>
                </Menu>
              </Container>
            </Segment>
            { children }
          </Sidebar.Pusher>
        </Sidebar.Pushable>
      </Responsive>
    )
  }
}

MobileContainer.propTypes = {
  children: PropTypes.node,
}

class DesktopContainer extends Component {
  state = {}

  hideFixedMenu = () => this.setState({ fixed: false })
  showFixedMenu = () => this.setState({ fixed: true })

  render() {
    const { url } = this.props
    const { fixed } = this.state
    const { children } = this.props
    return (
      <Responsive minWidth={Responsive.onlyTablet.minWidth}>
        <Visibility
          once={false}
          onBottomPassed={this.showFixedMenu}
          onBottomPassedReverse={this.hideFixedMenu}
        >
          <Segment
            inverted
            textAlign='center'
              style={{ background: "#FFF", padding:0 }}
            vertical
          >
            <Menu
              fixed={ fixed ? 'top' : null}
              inverted={true}
              pointing={!fixed}
              size='large'
            >
              <Container>
                {
                url == "/" ?

                  <Menu.Item as={Link} to="/" active>
                    Home
                  </Menu.Item>
                  :
                  <Menu.Item as={Link} to="/">
                    Home
                  </Menu.Item>
                }

                {
                  mylinks.map( (item,index) => {
                      if(url == item.link)
                        return ( <Menu.Item key={index} as={Link} to={item.link} active> {item.nombre} </Menu.Item> )
                      else {
                        return ( <Menu.Item key={index} as={Link} to={item.link}> {item.nombre} </Menu.Item> )
                      }
                    }
                  )
                }

                <Menu.Item position='right'>
                  <Button as='a' inverted={!fixed}>
                    Log in
                  </Button>
                  <Button as='a' inverted={!fixed} primary={fixed} style={{ marginLeft: '0.5em' }}>
                    Sign Up
                  </Button>
                </Menu.Item>
              </Container>
            </Menu>
          </Segment>
        </Visibility>

        {children}

      </Responsive>
    )
  }
}

DesktopContainer.propTypes = {
  children: PropTypes.node,
}

class ResponsiveContainer extends React.Component {
  render () {
    const { children } = this.props;
    const current_url = this.props.url ;
    const fixed = this.props.fixed ;

    const children_mobile = React.Children.map( children, child =>
      React.cloneElement(child, { mobile: true } )
    );

    return (
      <div>
        <DesktopContainer url={current_url} content={children} >{children}</DesktopContainer>
        <MobileContainer url={current_url}>{children_mobile}</MobileContainer>
      </div>
    );
  }
}

export {
  DesktopContainer,
  MobileContainer,
  ResponsiveContainer as default
}
