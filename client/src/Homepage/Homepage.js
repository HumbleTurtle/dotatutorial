import PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
  Button,
  Container,
  Divider,
  Grid,
  Header,
  Icon,
  Image,
  List,
  Menu,
  Responsive,
  Segment,
  Sidebar,
  Visibility,
} from 'semantic-ui-react'

class Page extends React.Component {
  render () {
    const  mobile  = this.props.mobile

    return (
      <Responsive
        style={{ background: "url('bg.jpg') right center #121212", backgroundRepeat:'no-repeat', backgroundSize: '80%', minHeight: 700, padding: '2em 0em'  }}
        >

        <Container text>
          <Header
            as='h1'
            content='Learn Dota 2 now'
            style={{
              fontSize: mobile ? '2em' : '4em',
              color:'#FFF', marginTop:'3em',
              fontWeight: 'normal',
              marginBottom:'0em',
              fontSize: '4em',
              textAlign:'center'
            }} />

        </Container>
      </Responsive>
    );
  }
}


export default Page;
