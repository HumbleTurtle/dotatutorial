import PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
  Button,
  Container,
  Divider,
  Grid,
  Header,
  Icon,
  Image,
  List,
  Menu,
  Responsive,
  Segment,
  Sidebar,
  Visibility,
} from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css';

import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import { DesktopContainer, MobileContainer } from './Container/ResponsiveContainer.js';
import Homepage from './Homepage/Homepage.js';
import Categories from './Categories/Categories.js';

class App extends React.Component {
  render () {
    return (
      <Router>
        <div>
          <Route path="/:any(.*)" component={ ( {match} ) => (
            <div>
              <MobileContainer url={match.url} fixed={true} >
                <Route exact path="/" render={()=>(<Homepage url={match.url} mobile={true} />)} />
                <Route exact path="/c/:category([A-z]+)" render={ (props) => (<Categories {...props} url={match.url} mobile={true} />)} />
                <Route exact path="/c/:category([A-z]+)/d/:difficulty/i/:id" render={ (props) => (<Categories {...props} url={match.url} mobile={true} />)} />
              </MobileContainer>

              <DesktopContainer url={match.url} fixed={true} >
                <Route exact path="/" render={()=>(<Homepage url={match.url} mobile={false} />)} />
                <Route exact path="/c/:category([A-z]+)" render={ (props) => (<Categories {...props} url={match.url} mobile={false} />)} />
                <Route exact path="/c/:category([A-z]+)/d/:difficulty/i/:id" render={ (props) => (<Categories {...props} url={match.url} mobile={false} />)} />
              </DesktopContainer>
            </div>
          )}>

          </Route>
        </div>
      </Router>
    );
  }
}

export default App
