import PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
  Button,
  Container,
  Divider,
  Grid,
  Header,
  Icon,
  Image,
  List,
  Menu,
  Responsive,
  Segment,
  Sidebar,
  Visibility,
} from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css';

import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import Cabecera from './Cabecera/Cabecera.js';
import Homepage from './Homepage/Homepage.js';

const App = () => (
  <Router>

    <Route exact path="/">
      <Responsive
        style={{ background: "url('bg.jpg') right center #121212", backgroundRepeat:'no-repeat', backgroundSize: '80%', minHeight: 700, padding: '1em 0em' }}
      >
        <Container>
        <Cabecera />

        </Container>
          <div>
            <hr />
            <Homepage />
          </div>

      </Responsive>
     </Route>

    <Responsive>
      <Container>
      <Cabecera />

      </Container>
        <div>
          <hr />
          <Route path="/about" component={Homepage} />
          <Route path="/topics" component={Homepage} />

        </div>

    </Responsive>
  </Router>

);


export default App
