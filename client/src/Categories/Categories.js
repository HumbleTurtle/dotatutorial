import PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
  Button,
  Container,
  Divider,
  Grid,
  Header,
  Icon,
  Image,
  List,
  Menu,
  Responsive,
  Segment,
  Sidebar,
  Visibility,
} from 'semantic-ui-react'

class Page extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      pasos: [],
      post: null,
      dificultad: 0,
      categoria_id: 0
    };

  }

  reloadPosts() {

    // Recargar el post si existe un id asociado al url
    if( this.state.post_id != null)
      // Obtener los datos
      return fetch('http://localhost:4000/api/categories/'+this.state.categoria_id+'/steps/'+this.state.dificultad)
        .then(response => response.json())
        .then(data => this.setState( { pasos: data.data } , () => {
          // Buscar el post solicitado y cargarlo
          for( var elem of this.state.pasos ) {
            if ( this.state.post_id == elem.id )
              this.loadPost(elem)
          }

        }) );

    return fetch('http://localhost:4000/api/categories/'+this.state.categoria_id+'/steps/'+this.state.dificultad)
      .then(response => response.json())
      .then(data => this.setState({ pasos: data.data } ));

  }

  preload() {
    const {url} = this.props
    const { category, difficulty, id} = this.props.match.params
    console.log(category + "  "+ id + " " + difficulty)
    var categoria_id = 0;

    switch ( category ) {
      case 'items':
        categoria_id = 2;
        break;

      case 'mechanics':
        categoria_id = 1;
        break;

      case 'strategies':
        categoria_id = 3;
        break;

    }



    this.setState( { dificultad: difficulty, categoria_id, post_id: id, post:null, pasos:[] } , () => { this.reloadPosts() } );
  }

  goBack() {
    this.props.history.goBack()
  }

  componentDidMount() {
    this.preload();
    this.goBack.bind(this)

    window.onpopstate  = (e) => {
      this.preload();
    }

  }

  loadPost (el) {
    const { category } = this.props.match.params

    this.setState( {post: el.datos, dificultad: el.dificultad} )
    this.props.history.push('/c/'+category+'/d/'+el.dificultad+'/i/'+el.id)
  }

  render () {
    var parent = this


    return (
     <Container
       style={{padding:'1em 0'}}
     >

       <Grid>
          <Grid.Column mobile={16} tablet={16} computer={16}>
             <Button.Group>
                <Button
                  positive= { this.state.dificultad == 0  }
                  onClick={ () => {
                    this.setState( { dificultad: 0 }, () => {this.reloadPosts()} );
                  } }
                >
                  Beginner
                </Button>

                <Button.Or text="<>"/>

                <Button
                  positive= { this.state.dificultad == 1  }
                  onClick= { () => {
                    this.setState( { dificultad: 1 }, () => {this.reloadPosts()}  );
                  } }
                >
                  Intermediate

                </Button>

                <Button.Or text="<>" />

                <Button
                  positive= { this.state.dificultad == 2  }
                  onClick= { () => {
                    this.setState( { dificultad: 2 }, () => {this.reloadPosts()}  );
                  } }
                >
                  Advanced
                </Button>
             </Button.Group>
          </Grid.Column>
         <Grid.Column mobile={16} tablet={4} computer={4}>
           <Button.Group basic vertical fluid >
             { this.state.pasos.map( (el, index) =>
                <Button fluid key={index} onClick={ this.loadPost.bind(this, el) }> {el.titulo} </Button>
              )
              }
              { this.state.pasos.length <= 0 &&
                <Button fluid disabled> No data available  </Button>
                }
           </Button.Group>
         </Grid.Column>
         <Grid.Column mobile={16} tablet={12} computer={12}>

           <Segment>
              { this.state.post ?
                <div dangerouslySetInnerHTML={{ __html: this.state.post }} />
                :
                <Image src='https://react.semantic-ui.com/images/wireframe/paragraph.png' />
              }

           </Segment>
         </Grid.Column>

       </Grid>

     </Container>
    );
  }
}




export default Page;
