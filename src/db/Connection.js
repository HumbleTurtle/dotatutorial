import mysql from 'mysql'

var port = process.env.PORT || 3000;

if (port === 3000) {

    var connection = mysql.createConnection({
      host: 'localhost',
      port: 3306,
      user: '',
      password: '',
      database: '',
      insecureAuth: true
    });
} else {

   //same as above, with live server details
}

connection.connect();

export default connection;
