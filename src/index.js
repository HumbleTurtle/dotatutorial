import express from 'express';
import cors from 'cors';
import CategoriasController from './controllers/CategoriasController'
import PasosController from './controllers/PasosController'

// Set up the express app
const app = express();

  app.use(cors())
  var basepath = '/api';

  app.route( basepath+'/categories')
    .get( (req, res, next) => { CategoriasController.getAllItems(req, res, next) } );

  app.route( basepath+'/categories/:id/steps/:dificultad')
    .get    ( (req, res, next) => { PasosController.getAllByDificultad(req, res, next) } );

  app.route( basepath+'/categories/:id')
    .get    ( (req, res, next) => { CategoriasController.getItem(req, res, next) } )
    .put    ( (req, res, next) => { CategoriasController.putItem(req, res, next) } );




  app.route( basepath+'/steps/:id')
    .get    ( (req, res, next) => { PasosController.getItem(req, res, next) } )
    .put    ( (req, res, next) => { PasosController.putItem(req, res, next) } )


  const PORT = 3000;

  app.listen(PORT, () => {
    console.log(`server running on port ${PORT}`)
  });

/*
const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => res.send('Hello World!'))

app.listen(port, () => console.log(`Example app listening on port ${port}!`)) */
