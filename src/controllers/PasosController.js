import Connection from '../db/Connection.js'

//create class
var controller = {
  tabla: "pasos",
  getItem: function (req, res) {
    var type = "element";
    //query the DB using prepared statement
    var results = Connection.query('SELECT * from pasos where id=??', req.params.id, function (error, results, fields) {
        //if error, print blank results
        if (error) {
          var apiResult = {
              type: "error"
          }

          res.json(apiResult);
        } else {
            //make results
            var resultJson = JSON.stringify(results);
            resultJson = JSON.parse(resultJson);
            var apiResult = {};

           // create a meta table to help apps
           //do we have results? what section? etc
            apiResult.meta = {
                table: this.tabla,
                type,
                total: req.params.id,
                total_entries: 0
            }

            //add our JSON results to the data table
            apiResult.data = resultJson;

            //send JSON to Express
            res.json(apiResult);
        }
    });
  },

  getAllByDificultad: function (req, res) {
      //query the DB using prepared statement
      var parent = this;
      var type = "collection";

      var results = Connection.query('SELECT * from ?? WHERE categorias_id=? && dificultad=?', [ parent.tabla, req.params.id, req.params.dificultad], function (error, results, fields) {
          //if error, print blank results
          if ( error ) {
              var apiResult = {
                  type: "error"
              }

              res.json(apiResult);
          } else {
            //make results
            var resultJson = JSON.stringify(results);
            resultJson = JSON.parse(resultJson);
            var apiResult = {};


           // create a meta table to help apps
           //do we have results? what section? etc
            apiResult.meta = {
                table: parent.tabla,
                type
            }

            //add our JSON results to the data table
            apiResult.data = resultJson;

            //send JSON to Express
            res.json(apiResult);
          }

      });
  },

};

export default controller;
