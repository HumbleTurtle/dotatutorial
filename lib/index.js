"use strict";

var _express = _interopRequireDefault(require("express"));

var _cors = _interopRequireDefault(require("cors"));

var _CategoriasController = _interopRequireDefault(require("./controllers/CategoriasController"));

var _PasosController = _interopRequireDefault(require("./controllers/PasosController"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Set up the express app
const app = (0, _express.default)();
app.use((0, _cors.default)());
var basepath = '/api';
app.route(basepath + '/categories').get((req, res, next) => {
  _CategoriasController.default.getAllItems(req, res, next);
});
app.route(basepath + '/categories/:id/steps/:dificultad').get((req, res, next) => {
  _PasosController.default.getAllByDificultad(req, res, next);
});
app.route(basepath + '/categories/:id').get((req, res, next) => {
  _CategoriasController.default.getItem(req, res, next);
}).put((req, res, next) => {
  _CategoriasController.default.putItem(req, res, next);
});
app.route(basepath + '/steps/:id').get((req, res, next) => {
  _PasosController.default.getItem(req, res, next);
}).put((req, res, next) => {
  _PasosController.default.putItem(req, res, next);
});
const PORT = 3000;
app.listen(PORT, () => {
  console.log(`server running on port ${PORT}`);
});
/*
const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => res.send('Hello World!'))

app.listen(port, () => console.log(`Example app listening on port ${port}!`)) */