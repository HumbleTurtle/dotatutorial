"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = function _default(app) {
  var strain = require('../controllers/CategoriasController');

  app.route('/categorias/id/:rowId').get(strain.getItemById).put(strain.updateItem).delete(strain.deleteItem);
};

exports.default = _default;