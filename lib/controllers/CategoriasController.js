"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _Connection = _interopRequireDefault(require("../db/Connection.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//create class
var controller = {
  tabla: "categorias",
  getItem: function (req, res) {
    var type = "element"; //query the DB using prepared statement

    var results = _Connection.default.query('SELECT * from categorias where id=?', req.params.id, function (error, results, fields) {
      //if error, print blank results
      if (error) {
        var apiResult = {
          type: "error"
        };
        res.json(apiResult);
      } else {
        //make results
        var resultJson = JSON.stringify(results);
        resultJson = JSON.parse(resultJson);
        var apiResult = {}; // create a meta table to help apps
        //do we have results? what section? etc

        apiResult.meta = {
          table: this.tabla,
          type,
          total: req.params.id,
          total_entries: 0 //add our JSON results to the data table

        };
        apiResult.data = resultJson; //send JSON to Express

        res.json(apiResult);
      }
    });
  },
  getAllItems: function (req, res) {
    //query the DB using prepared statement
    var parent = this;
    var type = "collection";

    var results = _Connection.default.query('SELECT * from categorias ORDER BY id', parent.tabla, function (error, results, fields) {
      //if error, print blank results
      if (error) {
        var apiResult = {
          type: "error-",
          data: error
        };
        res.json(apiResult);
      } else {
        //make results
        var resultJson = JSON.stringify(results);
        resultJson = JSON.parse(resultJson);
        var apiResult = {}; // create a meta table to help apps
        //do we have results? what section? etc

        apiResult.meta = {
          table: parent.tabla,
          type //add our JSON results to the data table

        };
        apiResult.data = resultJson; //send JSON to Express

        res.json(apiResult);
      }
    });
  }
};
var _default = controller;
exports.default = _default;